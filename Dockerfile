# Base
FROM node:18.7.0-alpine as base

ARG PORT=3000

WORKDIR /nuxtagram-frontend

RUN npm i -g pnpm

# Dependencies
FROM base as deps

RUN apk add --no-cache libc6-compat

COPY package.json pnpm-lock.yaml ./
RUN pnpm i --frozen-lockfile

# Build
FROM base as build

COPY --from=deps /nuxtagram-frontend/node_modules ./node_modules
COPY . .

RUN pnpm build 
RUN pnpm prune --prod

# Run
FROM base as runner

COPY --from=build /nuxtagram-frontend/.output ./.output

ENV HOST 0.0.0.0
EXPOSE 3000

CMD [ "node", ".output/server/index.mjs" ]