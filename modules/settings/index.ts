import {
  addComponentsDir,
  addImportsDir,
  defineNuxtModule,
  createResolver,
} from "@nuxt/kit";

export default defineNuxtModule({
  meta: {
    name: "settings",
  },
  defaults: {
    prefix: "Settings",
  },
  async setup() {
    const resolver = createResolver(import.meta.url);
    await addComponentsDir({
      path: resolver.resolve("./components"),
      prefix: "Settings",
      global: true,
    });
    addImportsDir(resolver.resolve("./composables"));
    addImportsDir(resolver.resolve("./content"));
  },
});
