export const howToUseLinks = [
  {
    label: "Edit settings",
    icon: "i-heroicons-user-circle",
    to: "/settings/edit",
  },
  {
    label: "Notifications",
    icon: "i-heroicons-bell-alert",
    to: "/settings/notifications",
  },
];

export const whatYouSeeLinks = [
  {
    label: "Muted accounts",
    icon: "i-heroicons-bell-slash",
    to: "/settings/muted_accounts",
  },
  {
    label: "Like and share counts",
    icon: "i-heroicons-hand-thumb-up",
    to: "/settings/like_count",
  },
  {
    label: "Subscriptions",
    icon: "i-mdi-crown",
    to: "/settings/subscriptions",
  },
];

export const whoCanSeeYourContentLinks = [
  {
    label: "Account privacy",
    icon: "i-heroicons-lock-closed",
    to: "/settings/privacy_setting",
  },
  {
    label: "Close Friends",
    icon: "i-heroicons-star",
    to: "/settings/close_friends",
  },
  {
    label: "Blocked",
    icon: "i-heroicons-no-symbol",
    to: "/settings/blocked_accounts",
  },
  {
    label: "Hide story and live",
    icon: "i-heroicons-eye-slash",
    to: "/settings/hide_story_and_live",
  },
];
