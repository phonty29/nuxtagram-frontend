export enum ICreateModalStep {
  SELECT = "select",
  CROP = "crop",
  EDIT = "edit",
  SHARE = "share",
}
