export interface ICreatePostRequest {
  picture: File;
  caption?: string;
  location?: string;
  alt_text?: string;
  hide_likes?: boolean;
  turn_off_comments?: boolean;
}

export interface ICreatePostResponse {
  picture: string;
  owner: string;
  created_at: Date;
  location: string;
  caption?: string;
  alt_text?: string;
  hide_likes?: boolean;
  turn_off_comments?: boolean;
}
