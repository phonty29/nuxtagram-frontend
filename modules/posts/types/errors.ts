import type { IBaseError } from "types/errors";

export interface ICreatePostErrors extends IBaseError {
  picture: [string];
  caption?: [string];
  location?: [string];
  alt_text?: [string];
  hide_likes?: [string];
  turn_off_comments?: [string];
}
