import {
  addComponentsDir,
  addImportsDir,
  defineNuxtModule,
  createResolver,
} from "@nuxt/kit";

export default defineNuxtModule({
  meta: {
    name: "posts",
  },
  defaults: {
    prefix: "Posts",
  },
  async setup() {
    const resolver = createResolver(import.meta.url);
    await addComponentsDir({
      path: resolver.resolve("./components"),
      prefix: "Posts",
      global: true,
    });
    addImportsDir(resolver.resolve("./composables"));
    addImportsDir(resolver.resolve("./http-service"));
    addImportsDir(resolver.resolve("./types"));
  },
});
