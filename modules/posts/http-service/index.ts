export class PostsService {
  static async createNewPost(createPostData: ICreatePostRequest) {
    const body = new FormData();
    body.append("picture", createPostData.picture);
    Object.entries(createPostData).forEach(([key, value]) => {
      if (
        key !== "picture" &&
        value &&
        (typeof value === "string" || typeof value === "boolean")
      )
        body.append(key, typeof value === "boolean" ? value.toString() : value);
    });

    const { data, error } = await useNuxtagramFetch<ICreatePostResponse>(
      "/posts/all",
      {
        method: "POST",
        body,
      },
    );
    if (data.value) {
      return data.value;
    } else if (error.value) {
      usePostsErrorsNotification().notificateCreationPostErrors(
        error.value.data as Partial<ICreatePostErrors>,
      );
    }
  }
}
