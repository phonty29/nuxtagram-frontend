export const isCreateModalOpen = ref(false);

export const openCreateModal = () => {
  isCreateModalOpen.value = true;
};
export const closeCreateModal = () => {
  isCreateModalOpen.value = false;
};
