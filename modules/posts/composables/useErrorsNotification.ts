class PostsErrorsNotification extends ErrorsNotification {
  public notificateCreationPostErrors(errorData: Partial<ICreatePostErrors>) {
    if (errorData.detail) this.handleExceptionErrors(errorData.detail);
    if (errorData.picture?.length) this.showKnownErrors(errorData.picture);
    if (errorData.caption?.length) this.showKnownErrors(errorData.caption);
    if (errorData.alt_text?.length) this.showKnownErrors(errorData.alt_text);
    if (errorData.location?.length) this.showKnownErrors(errorData.location);
    if (errorData.hide_likes?.length)
      this.showKnownErrors(errorData.hide_likes);
    if (errorData.turn_off_comments?.length)
      this.showKnownErrors(errorData.turn_off_comments);
    this.showUnknownError();
  }
}

export const usePostsErrorsNotification = () => {
  return new PostsErrorsNotification();
};
