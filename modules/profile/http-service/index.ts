export class ProfileService {
  static async getProfile() {
    const { data, error } = await useNuxtagramFetch<IProfileResponse>(
      "/auth/profile",
      {
        method: "GET",
      },
    );

    if (data.value) {
      return data.value;
    } else if (error.value) {
      useProfileErrorsNotification().notificateProfileErrors(
        error.value.data as Partial<IProfileErrors>,
      );
    }
  }

  static async changeProfileImage(imageFile: File) {
    const body = new FormData();
    body.append("profile_img", imageFile);

    const { data, error } = await useNuxtagramFetch<IProfileResponse>(
      "/auth/profile",
      {
        method: "PATCH",
        body,
      },
    );

    if (data.value) {
      return data.value.profile_img;
    } else if (error.value) {
      useProfileErrorsNotification().notificateProfileErrors(
        error.value.data as Partial<IProfileErrors>,
      );
    }
  }

  static async removeCurrentPhoto() {
    const body = new FormData();
    body.append("profile_img", "");

    const { data, error } = await useNuxtagramFetch<IProfileResponse>(
      "/auth/profile",
      {
        method: "PATCH",
        body,
      },
    );

    if (data.value) {
      return data.value.profile_img;
    } else if (error.value) {
      useProfileErrorsNotification().notificateProfileErrors(
        error.value.data as Partial<IProfileErrors>,
      );
    }
  }

  static async updateProfileData(profileData: IProfileRequest) {
    const { data, error } = await useNuxtagramFetch<IProfileResponse>(
      "/auth/profile",
      {
        method: "PATCH",
        body: profileData,
      },
    );

    if (data.value) {
      return data.value;
    } else if (error.value) {
      useProfileErrorsNotification().notificateProfileErrors(
        error.value.data as Partial<IProfileErrors>,
      );
    }
  }
}
