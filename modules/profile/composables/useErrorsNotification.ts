class ProfileErrorsNotification extends ErrorsNotification {
  public notificateProfileErrors(errorData: Partial<IProfileErrors>) {
    if (errorData.detail) this.handleExceptionErrors(errorData.detail);
    if (errorData.user) {
      if (errorData.user.username?.length)
        this.showKnownErrors(errorData.user.username);
      if (errorData.user.email?.length)
        this.showKnownErrors(errorData.user.email);
      if (errorData.user.phone?.length)
        this.showKnownErrors(errorData.user.phone);
      if (errorData.user.full_name?.length)
        this.showKnownErrors(errorData.user.full_name);
    }
    if (errorData.bio?.length) this.showKnownErrors(errorData.bio);
    if (errorData.gender?.length) this.showKnownErrors(errorData.gender);
    if (errorData.profile_img?.length)
      this.showKnownErrors(errorData.profile_img);
    if (errorData.show_suggestions?.length)
      this.showKnownErrors(errorData.show_suggestions);
    if (errorData.website_link?.length)
      this.showKnownErrors(errorData.website_link);
    this.showUnknownError();
  }
}

export const useProfileErrorsNotification = () => {
  return new ProfileErrorsNotification();
};
