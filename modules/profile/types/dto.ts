import type { IGenderOptions } from "./enums";
import type { IUser } from "accounts";

export interface IProfileRequest {
  bio: string;
  website_link: string;
  gender: IGenderOptions;
  show_suggestions: boolean;
}

export interface IProfileResponse {
  user: IUser;
  bio: string;
  website_link: string;
  profile_img: string;
  gender: IGenderOptions;
  show_suggestions: boolean;
}
