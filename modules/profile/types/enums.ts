export enum IGenderOptions {
  MALE = "M",
  FEMALE = "F",
  OTHERS = "O",
}
