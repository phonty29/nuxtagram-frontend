import type { IBaseError } from "types/errors";

export interface IProfileErrors extends IBaseError {
  user: {
    username: [string];
    email: [string];
    phone: [string];
    full_name: [string];
  };
  bio: [string];
  website_link: [string];
  profile_img: [string];
  gender: [string];
  show_suggestions: [string];
}
