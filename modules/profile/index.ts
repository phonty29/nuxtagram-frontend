import {
  addComponentsDir,
  addImportsDir,
  defineNuxtModule,
  createResolver,
} from "@nuxt/kit";

export default defineNuxtModule({
  meta: {
    name: "profile",
  },
  defaults: {
    prefix: "Profile",
  },
  async setup() {
    const resolver = createResolver(import.meta.url);
    await addComponentsDir({
      path: resolver.resolve("./components"),
      prefix: "Profile",
      global: true,
    });
    addImportsDir(resolver.resolve("./composables"));
    addImportsDir(resolver.resolve("./types"));
    addImportsDir(resolver.resolve("./store"));
    addImportsDir(resolver.resolve("./http-service"));
  },
});
