import { defineStore } from "pinia";

export const useProfileStore = defineStore("profile-store", () => {
  const profile = ref<IProfileResponse>();

  async function getProfile() {
    if (profile.value === undefined) {
      const profileData = await ProfileService.getProfile();
      if (profileData !== undefined) {
        setProfile(profileData);
      }
    }

    return profile.value;
  }

  function setProfile(profileData: IProfileResponse) {
    profile.value = profileData;
  }

  return {
    getProfile,
    setProfile,
  };
});
