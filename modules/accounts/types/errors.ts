import type { IBaseError } from "types/errors";

export interface IRegistrationErrors extends IBaseError {
  user: {
    username: [string];
    email: [string];
    phone: [string];
    full_name: [string];
  };
  password: [string];
}

export interface ICredentialsErrors extends IBaseError {
  auth_field: [string];
  password: [string];
}

export interface ILoginProfileErrors extends IBaseError {
  credentials: ICredentialsErrors;
}

export interface ISocialLoginErrors extends IBaseError {
  auth_token: [string];
}

export interface IProfileActivationErrors extends IBaseError {
  credentials: ICredentialsErrors;
  activation_code: [string];
}
