export interface ICredentialsRequest {
  auth_field: string;
  password: string;
}
export interface ICredentialsResponse {
  token: string;
  is_created: boolean;
  last_login: Date;
}

export interface IUser {
  username: string;
  email: string;
  phone: string;
  full_name: string;
}
