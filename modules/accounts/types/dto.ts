import type {
  IUser,
  ICredentialsRequest,
  ICredentialsResponse,
} from "./entities";

export interface IRegistrationRequest {
  user: IUser;
  password: string;
  confirm_password: string;
}
export interface IRegistrationResponse {
  user: IUser;
}

export interface IActivationCodeRequest {
  credentials: ICredentialsRequest;
}
export interface IActivationCodeResponse {
  is_send: boolean;
}

export interface IActivationProfileRequest {
  credentials: ICredentialsRequest;
  activation_code: number;
}
export interface IActivationProfileResponse {
  credentials: ICredentialsResponse;
  is_activated: boolean;
}

export interface ILoginRequest {
  credentials: ICredentialsRequest;
}
export interface ILoginResponse {
  credentials: ICredentialsResponse;
}

export interface ISocialRequest {
  auth_token: string | undefined;
}
