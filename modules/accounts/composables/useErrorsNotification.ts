import type {
  ICredentialsErrors,
  ILoginProfileErrors,
  IProfileActivationErrors,
  IRegistrationErrors,
  ISocialLoginErrors,
} from "../types";
import type { IBaseError } from "types/errors";

class AccountErrorsNotification extends ErrorsNotification {
  public notificateGetProfileErrors(errorData: IBaseError) {
    if (errorData.detail) this.handleExceptionErrors(errorData.detail);
  }

  public notificateRegistrationErrors(errorData: Partial<IRegistrationErrors>) {
    if (errorData.detail) this.handleExceptionErrors(errorData.detail);
    if (errorData.user) {
      if (errorData.user.username?.length)
        this.showKnownErrors(errorData.user.username);
      if (errorData.user.email?.length)
        this.showKnownErrors(errorData.user.email);
      if (errorData.user.phone?.length)
        this.showKnownErrors(errorData.user.phone);
      if (errorData.user.full_name?.length)
        this.showKnownErrors(errorData.user.full_name);
    }
    if (errorData.password) this.showKnownErrors(errorData.password);
    this.showUnknownError();
  }

  public notificateCredentialsErrors(credentials: ICredentialsErrors) {
    if (credentials.auth_field?.length)
      this.showKnownErrors(credentials.auth_field);
    if (credentials.password?.length)
      this.showKnownErrors(credentials.password);
  }

  public notificateLoginProfileErrors(errorData: Partial<ILoginProfileErrors>) {
    if (errorData.detail) this.handleExceptionErrors(errorData.detail);
    if (errorData.credentials)
      this.notificateCredentialsErrors(errorData.credentials);
    this.showUnknownError();
  }

  public notificateSocialLoginErrors(errorData: Partial<ISocialLoginErrors>) {
    if (errorData.detail) this.handleExceptionErrors(errorData.detail);
    if (errorData.auth_token)
      if (errorData.auth_token?.length)
        this.showKnownErrors(errorData.auth_token);
    this.showUnknownError();
  }

  public notificateProfileActivationErrors(
    errorData: Partial<IProfileActivationErrors>,
  ) {
    if (errorData.detail) this.handleExceptionErrors(errorData.detail);
    if (errorData.credentials)
      this.notificateCredentialsErrors(errorData.credentials);
    if (errorData.activation_code)
      this.showKnownErrors(errorData.activation_code);
    this.showUnknownError();
  }
}

export const useAccountsErrorsNotification = () => {
  return new AccountErrorsNotification();
};
