import { defineStore } from "pinia";
import type { IActivationCodeRequest } from "../types";

export const useActivateProfileStore = defineStore(
  "activate-profile-store",
  () => {
    const getActivationCodeData = ref<IActivationCodeRequest>();

    function setActivationCodeData(activationCodeData: IActivationCodeRequest) {
      getActivationCodeData.value = activationCodeData;
    }

    return {
      getActivationCodeData,
      setActivationCodeData,
    };
  },
);
