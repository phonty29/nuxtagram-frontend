import {
  addComponentsDir,
  addImportsDir,
  defineNuxtModule,
  createResolver,
} from "@nuxt/kit";

export default defineNuxtModule({
  meta: {
    name: "accounts",
  },
  defaults: {
    prefix: "Accounts",
  },
  async setup() {
    const resolver = createResolver(import.meta.url);
    await addComponentsDir({
      path: resolver.resolve("./components"),
      prefix: "Accounts",
      global: true,
    });
    addImportsDir(resolver.resolve("./composables"));
    addImportsDir(resolver.resolve("./http-service"));
    addImportsDir(resolver.resolve("./store"));
    addImportsDir(resolver.resolve("./types"));
  },
});
