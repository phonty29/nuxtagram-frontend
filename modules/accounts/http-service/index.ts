import type {
  IRegistrationRequest,
  IRegistrationResponse,
  IActivationCodeRequest,
  IRegistrationErrors,
  ILoginRequest,
  ILoginResponse,
  ILoginProfileErrors,
  ISocialRequest,
  ISocialLoginErrors,
  IActivationCodeResponse,
  IActivationProfileRequest,
  IActivationProfileResponse,
  IProfileActivationErrors,
} from "../types";

export class AccountsService {
  static async registrateUser(registrationData: IRegistrationRequest) {
    const { status, error } = await useNuxtagramFetch<IRegistrationResponse>(
      "/auth/users/registration",
      {
        method: "POST",
        body: registrationData,
        watch: false,
      },
    );

    if (status.value === "success") {
      const activationCodeData: IActivationCodeRequest = {
        credentials: {
          auth_field: registrationData.user.email,
          password: registrationData.password,
        },
      };
      useActivateProfileStore().setActivationCodeData(activationCodeData);
      const isSend = await this.sendActivationCode(activationCodeData);
      return { isCreated: true, isSend };
    } else if (error.value) {
      useAccountsErrorsNotification().notificateRegistrationErrors(
        error.value.data as Partial<IRegistrationErrors>,
      );
    }

    return { isCreated: false, isSend: false };
  }

  static async loginProfile(loginData: ILoginRequest) {
    const { data, error } = await useNuxtagramFetch<ILoginResponse>(
      "/auth/profile/login",
      {
        method: "POST",
        body: loginData,
        watch: false,
      },
    );

    if (data.value) {
      localStorage.setItem("authorization", data.value.credentials.token);
      return { isAuthenticated: true };
    } else if (error.value) {
      useAccountsErrorsNotification().notificateLoginProfileErrors(
        error.value.data as Partial<ILoginProfileErrors>,
      );
    }

    return { isAuthenticated: false };
  }

  static async googleLogin(socialData: ISocialRequest) {
    const { data, error } = await useNuxtagramFetch<ILoginResponse>(
      "/auth/social/google",
      {
        method: "POST",
        body: socialData,
      },
    );

    if (data.value) {
      localStorage.setItem("authorization", data.value.credentials.token);
      return { isAuthenticated: true };
    } else if (error.value) {
      useAccountsErrorsNotification().notificateSocialLoginErrors(
        error.value.data as Partial<ISocialLoginErrors>,
      );
    }

    return { isAuthenticated: false };
  }

  static async facebookLogin(socialData: ISocialRequest) {
    const { data, error } = await useNuxtagramFetch<ILoginResponse>(
      "/auth/social/facebook",
      {
        method: "POST",
        body: socialData,
      },
    );

    if (data.value) {
      localStorage.setItem("authorization", data.value.credentials.token);
      return { isAuthenticated: true };
    } else if (error.value) {
      useAccountsErrorsNotification().notificateSocialLoginErrors(
        error.value.data as Partial<ISocialLoginErrors>,
      );
    }

    return { isAuthenticated: false };
  }

  static async sendActivationCode(activationCodeData: IActivationCodeRequest) {
    const { data } = await useNuxtagramFetch<IActivationCodeResponse>(
      "/auth/profile/activation/code",
      {
        method: "POST",
        body: activationCodeData,
      },
    );
    if (data.value) return data.value.is_send;

    return false;
  }

  static async activateProfile(
    activationProfileData: IActivationProfileRequest,
  ) {
    const { data, error } = await useNuxtagramFetch<IActivationProfileResponse>(
      "/auth/profile/activation",
      {
        method: "POST",
        body: activationProfileData,
      },
    );

    if (data.value) {
      localStorage.setItem("authorization", data.value.credentials.token);
      return { isActivated: data.value.is_activated };
    } else if (error.value) {
      useAccountsErrorsNotification().notificateProfileActivationErrors(
        error.value.data as Partial<IProfileActivationErrors>,
      );
    }

    return { isActivated: false };
  }
}
