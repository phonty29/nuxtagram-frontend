import uiConfig from "./ui.config";
import { defineAppConfig } from "#imports";

export default defineAppConfig({
  ui: {
    ...uiConfig,
  },
});
