interface INotificationOptions {
  title: string;
  description?: string;
  timeout?: number;
}

export default defineNuxtPlugin(() => {
  const toast = useToast();

  class Notification {
    showSuccess(options: INotificationOptions) {
      toast.add({
        ...options,
        icon: "i-heroicons-check-circle",
        color: "green",
        timeout: 3000,
      });
    }

    showError(options: INotificationOptions) {
      toast.add({
        ...options,
        icon: "i-heroicons-x-circle",
        color: "red",
        timeout: 3000,
      });
    }

    showInfo(options: INotificationOptions) {
      toast.add({
        ...options,
        icon: "i-heroicons-exclamation-circle",
        color: "sky",
        timeout: 3000,
      });
    }
  }

  return {
    provide: {
      notification: new Notification(),
    },
  };
});
