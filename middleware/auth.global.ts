const allowedRoutes = [
  "accounts-login",
  "accounts-emailsignup",
  "accounts-activation",
];

export default defineNuxtRouteMiddleware(async (to, _from) => {
  if (allowedRoutes.includes(to.name as string)) return;

  const profile = await useProfileStore().getProfile();
  if (!profile && process.client) {
    return navigateTo("/accounts/login");
  }
});
