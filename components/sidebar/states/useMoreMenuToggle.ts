export function useMoreMenuToggle() {
  return useState<boolean>("isMoreMenuOpened", () => false);
}
