export default [
  {
    label: "Home",
    to: "/",
    name: "index",
    icon: {
      outline: "i-heroicons-home",
      solid: "i-heroicons-home-solid",
    },
  },
  {
    label: "Profile",
    to: "/profile",
    name: "profile",
    avatar: true,
  },
  {
    label: "Explore",
    to: "/explore",
    name: "explore",
    icon: {
      outline: "i-heroicons-globe-americas",
      solid: "i-heroicons-globe-americas-solid",
    },
  },
  {
    label: "Messenger",
    to: "/messenger",
    name: "messenger",
    icon: {
      outline: "i-heroicons-inbox",
      solid: "i-heroicons-inbox-solid",
    },
  },
  {
    label: "Reels",
    to: "/reels",
    name: "reels",
    icon: {
      outline: "i-heroicons-film",
      solid: "i-heroicons-film-solid",
    },
  },
  {
    label: "Notifications",
    name: "notifications",
    action: "openNotificationsSlideover",
    icon: {
      outline: "i-heroicons-heart",
      solid: "i-heroicons-heart-solid",
    },
  },
  {
    label: "Create",
    name: "create",
    action: "openCreateModal",
    icon: {
      outline: "i-heroicons-plus-circle",
      solid: "i-heroicons-plus-circle-solid",
    },
  },
  {
    label: "More",
    name: "more",
    action: "toggleMoreMenu",
    icon: {
      outline: "i-heroicons-bars-3",
      solid: "i-heroicons-bars-3-solid",
    },
  },
];
