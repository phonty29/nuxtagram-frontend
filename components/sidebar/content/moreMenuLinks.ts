export default [
  {
    label: "Settings",
    to: "/settings/edit",
    name: "settings",
    icon: "i-heroicons-cog-6-tooth",
  },
  {
    label: "Your activity",
    to: "/activity",
    name: "activity",
    icon: "i-heroicons-clock",
  },
  {
    label: "Saved",
    to: "/saved",
    name: "saved",
    icon: "i-heroicons-bookmark",
  },
  {
    label: "Report a problem",
    to: "/report",
    name: "report",
    icon: "i-heroicons-flag",
  },
];
