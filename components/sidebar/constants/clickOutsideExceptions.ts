enum ClickOutsideExpceptions {
  MORE_LINK = "a.more",
}

export default ClickOutsideExpceptions;
