import { fileURLToPath } from "url";

export default defineNuxtConfig({
  app: {
    head: {
      title: "Nuxtagram",
    },
  },
  modules: [
    "@nuxt/ui",
    "@pinia/nuxt",
    "@nuxtjs/eslint-module",
    "nuxt-vue3-google-signin",
    "@vueuse/nuxt",
  ],
  ui: {
    global: true,
  },
  devtools: { enabled: false },
  googleSignIn: {
    clientId: process.env.GOOGLE_CLIENT_ID,
  },
  css: ["vue3-emoji-picker/css"],
  pinia: {
    autoImports: ["defineStore", ["defineStore", "definePiniaStore"]],
  },
  components: {
    dirs: ["@/components"],
  },
  alias: {
    types: fileURLToPath(new URL("./types", import.meta.url)),
    accounts: fileURLToPath(new URL("./modules/accounts", import.meta.url)),
    profile: fileURLToPath(new URL("./modules/profile", import.meta.url)),
    settings: fileURLToPath(new URL("./modules/settings", import.meta.url)),
    posts: fileURLToPath(new URL("./modules/posts", import.meta.url)),
  },
  runtimeConfig: {
    public: {
      baseURL: process.env.NUXT_PUBLIC_API_BASE,
    },
  },
  tailwindcss: {
    cssPath: "@/assets/css/tailwind.css",
    configPath: "tailwind.config.ts",
    viewer: true,
  },
  devServer: {
    https: {
      key: "certs/dev.key",
      cert: "certs/dev.crt",
    },
  },
  vite: {
    vue: {
      script: {
        defineModel: true,
        propsDestructure: true,
      },
    },
    optimizeDeps: { exclude: ["fsevents"] },
  },
});
