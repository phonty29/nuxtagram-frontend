export const footerLinks = [
  {
    to: "https://google.com",
    label: "Organization",
    id: 1,
  },
  {
    to: "https://google.com",
    label: "About",
    id: 2,
  },
  {
    to: "https://google.com",
    label: "Blog",
    id: 3,
  },
  {
    to: "https://google.com",
    label: "Jobs",
    id: 4,
  },
  {
    to: "https://google.com",
    label: "Help",
    id: 5,
  },
  {
    to: "https://google.com",
    label: "API",
    id: 6,
  },
  {
    to: "https://google.com",
    label: "Privacy",
    id: 7,
  },
  {
    to: "https://google.com",
    label: "Terms",
    id: 8,
  },
  {
    to: "https://google.com",
    label: "Terms",
    id: 9,
  },
  {
    to: "https://google.com",
    label: "Locations",
    id: 10,
  },
  {
    to: "https://google.com",
    label: "Contact Uploading & Non-Users",
    id: 11,
  },
  {
    to: "https://google.com",
    label: "Nuxtagram Verified",
    id: 12,
  },
];
