export const availableLanguages = [
  {
    name: "Русский",
    value: "ru-RU",
  },
  {
    name: "English(US)",
    value: "en-US",
  },
  {
    name: "Enlgish(UK)",
    value: "en-GB",
  },
  {
    name: "Қазақша",
    value: "kz-KZ",
  },
];
