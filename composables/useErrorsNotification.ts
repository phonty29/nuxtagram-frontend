export class ErrorsNotification {
  public notification;
  public isUnknownError: boolean;

  public constructor() {
    const { $notification } = useNuxtApp();
    this.notification = $notification;
    this.isUnknownError = true;
  }

  private formatServerError(err: string): string {
    return `Error: ${err}`;
  }

  private showErrors(errors: string[]) {
    errors.forEach((err: string) => {
      this.notification.showError({
        title: this.formatServerError(err),
      });
    });
  }

  public handleExceptionErrors(detail: string) {
    this.isUnknownError = false;
    if (detail === "user_is_none") {
      this.showErrors([
        "Invalid username or password. Please ensure your credentials are correct and try again.",
      ]);
      return;
    }
    if (detail === "user_is_inactive") {
      this.showErrors([
        "User is not active. Please activate your profile and try again.",
      ]);
      return;
    }
    if (detail === "invalid_activation_code") {
      this.showErrors([
        "Entered activation code is not correct. Please check out your email or resend activation code.",
      ]);
      return;
    }
    this.showErrors([detail]);
  }

  public showKnownErrors(errors: string[]) {
    this.isUnknownError = false;
    if (this.isUnknownError) {
      this.showErrors(errors);
    }
  }

  public showUnknownError() {
    if (this.isUnknownError) {
      this.showErrors(["unknown backend error"]);
    }
  }
}
