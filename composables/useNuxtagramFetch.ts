import type { UseFetchOptions } from "nuxt/app";

export async function useNuxtagramFetch<T>(
  url: string,
  options: UseFetchOptions<T> = {},
) {
  const config = useRuntimeConfig();

  return await useFetch(url, {
    baseURL: config.public.baseURL,
    credentials: "same-origin",
    server: false,
    onRequest({ options }) {
      options.headers = options.headers || {};
      const authorizationToken = localStorage.getItem("authorization") || "";
      options.headers = {
        ...options.headers,
        authorization: `Token ${authorizationToken}`,
      };
    },
    ...options,
  });
}
