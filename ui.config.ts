export default {
  colorMode: {
    preference: "light",
  },
  notifications: {
    position: "top-0 bottom-auto",
  },
  icons: {
    dynamic: true,
  },
};
